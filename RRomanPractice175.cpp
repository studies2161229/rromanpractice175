



#include <iostream>

class Vector
{
public:
    Vector()
    {}
    void Show()
    {
        std::cout << "\n\n     " << x << " " << y << " " << z << "\n\n";
    }
    void VectorModule()
    {
        std::cout << "\n\n     " << "3d Vector Magnitude:\n\n     " << "sqrt((x * x) + (y * y) + (z * z)) = " << sqrt((x * x) + (y * y) + (z * z)) << "\n\n";
    }
private:
    double x = -3;
    double y = 4;
    double z = 5;
};


int main()
{
    Vector v;
    v.Show();
    v.VectorModule();
}
